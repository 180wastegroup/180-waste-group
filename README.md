The 180 Waste Group supply healthcare waste management equipment to best manage solid and liquid healthcare trade waste. The compact onsite technology uses frictional heat treatment to process potentially infectious clinical waste. The easy to use equipment produces a byproduct that is sterilised, dehydrated, odourless and easily disposed.

Website: https://180wastegroup.com

